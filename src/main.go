package main

import (
	"github.com/gin-gonic/gin"
	crowler "gotest/src/crowler"
	"gotest/src/models"
	"net/http"
)

func main() {
	r := gin.Default()
	dataBase := models.DataBase{}
	dataBase.ConnectDatabase()

	r.GET("/", func(c *gin.Context) {
		results:= dataBase.SelectCounts()
		c.JSON(http.StatusOK, gin.H{"results": &results})
	})

	r.POST("/", func(c *gin.Context) {
		count := crowler.CrowlData()
		dataBase.InsertCounts(count)
		c.JSON(http.StatusOK, gin.H{"data": "Done"})
	})

	r.Run()
}