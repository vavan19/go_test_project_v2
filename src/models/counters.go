package models

type SearchRun struct {
	ID     uint   `json:"id" gorm:"primary_key"`
	Count  int `json:"count"`
}

type SearchRunsTotal struct {
	Total int64 `json:"total"`
}