package models

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DataBase struct {
	DB *gorm.DB
}

func (db *DataBase) ConnectDatabase() {
	dsn := "host=localhost user=postgres password=12345 dbname=go_test port=5432 sslmode=disable"
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	database.AutoMigrate(&SearchRun{})

	db.DB = database
}

func (db DataBase) SelectCounts() (SearchRunsTotal)  {
	var total SearchRunsTotal
	db.DB.Table("search_runs").Select("sum(count) as total").Scan(&total)
	return total
}

func (db DataBase) InsertCounts(count int) (error) {
	model := SearchRun{Count: count}

	result := db.DB.Create(&model)
	return result.Error
}