package google_crowler

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
)

const filepath = "/Users/ivanganenko/work/go_test/src/gotest/src/google_crowler/words.txt"
const searchWord = "google"

type SafeCounter struct {
	mu    sync.Mutex
	count int
}

type DataSource interface {
	GetData(string) (string, error)
}

func Run(source DataSource) (int) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	queryCh := make(chan string, 30)
	var counter SafeCounter
	scanner := bufio.NewScanner(file)
	wg := &sync.WaitGroup{}
	for i := 0; i < 10; i++ {
		go processPage(queryCh, &counter, wg, source)
		wg.Add(1)
	}

	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			log.Println(err)
			continue
		}
		queryCh <- scanner.Text()
	}
	close(queryCh)

	wg.Wait()

	return counter.count
}

func processPage(queryCh chan string, counter *SafeCounter, wg *sync.WaitGroup, source DataSource) {
	defer wg.Done()
	for query := range queryCh {
		fmt.Println(query)
		html, err := source.GetData(query)
		if err != nil {
			log.Println(fmt.Sprintf(`error reading from google: %v`, err))
			continue
		}

		wordsCount := occurrences(html)

		counter.mu.Lock()
		counter.count += wordsCount
		counter.mu.Unlock()
	}
}

func occurrences(html string) int {
	wordsCount := strings.Count(html, searchWord)

	return wordsCount
}
