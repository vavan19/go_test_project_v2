package crowler

import (
	"fmt"
	"gotest/src/google_crowler"
	"io/ioutil"
	"net/http"
)

type GoogleDataSource struct {
	queryFormat string
}

func (g GoogleDataSource) GetData(s string) (string, error) {
	resp, err := http.Get(fmt.Sprintf(g.queryFormat, s))
	if err != nil {
		return ``, err
	}
	defer resp.Body.Close()

	bites, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ``, err
	}

	return string(bites), nil
}

func CrowlData() (int) {
	source := GoogleDataSource{
		queryFormat: `http://www.google.com/search?q=%v`,
	}
	return google_crowler.Run(source)
	//fmt.Println(google_crowler.Run(source))
}